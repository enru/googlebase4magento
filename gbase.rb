#!/usr/bin/ruby

# pulls products & attributes from the Magento DB 
# formats & (optionally) sends the results to Google Base FTP servers

# author: Neill Russell <n@enru.co.uk>
# author: Inigo Media Ltd <hello@inigo.net>

# written for a specific Magento customer at Inigo Media Ltd
# but could easily be modified for use with other set-ups

# google column spec
# http://support.google.com/merchants/bin/answer.py?hl=en-GB&answer=1344057#GB

# google product categories
# http://support.google.com/merchants/bin/answer.py?hl=en&answer=160081

require 'mysql'
require 'csv'
require 'benchmark'
require 'net/ftp'

# CONFIG
send_file = true 
user, pass, dbname = 'magento', 'password', 'magento' 
$base_url = "http://www.example.com/"
csv_file = 'gbase.txt'

# GOOGLE BASE FTP:
google = {
	:host => 'uploads.google.com',
	:user => 'gbase_user',
	:pass => 'password',
}

$keys = [
	:id,
	:title,
	:description,
	:google_product_category,
	:link,
	:image_link, 
	:price,
	:brand,
	:gender,
	:color,
	:condition, 
	:availability,
	:product_type,
	:shipping_weight,
	:shipping,
	#:item_group_id,
	#:size, # TODO,
]

$old_keys = Array.new($keys.length)
$old_keys[1] = :name
$old_keys[2] = :short_description
$old_keys[4] = :url_key
$old_keys[5] = :image
$old_keys[13] = :weight

$product_type_map = {
		'Wallets' => 'Apparel & Accessories > Wallets & Money Clips',
		'Watches' => 'Apparel & Accessories > Jewelry > Watches',
		'Headphones' => 'Electronics > Audio > Audio Components > Headphones',
		'iPhone Cases' => 'Electronics > Audio > Audio Accessories > ' +
			'MP3 Player Accessories > MP3 Player Cases',
		'Laptop Sleeves' => 'Electronics > Computers > Computer Accessories',
}

def get_int_attr(attr_code, entity_id)
	res = $dbh.query("
	select attr_int_opt_val.value 
	from catalog_product_entity_int as entity_int
	join eav_attribute as attr_int
	on attr_int.attribute_id = entity_int.attribute_id
	and attr_int.attribute_code = '" + $dbh.escape_string(attr_code)+"'
	left join eav_attribute_option_value as attr_int_opt_val
	on attr_int_opt_val.option_id = entity_int.value
	where entity_int.entity_id = " + $dbh.escape_string(entity_id))
	if row = res.fetch_row then row.shift end
end

def get_varchar_attr(attr_code, entity_id)
	res = $dbh.query("
	select entity_varchar.value 
	from catalog_product_entity_varchar as entity_varchar 
	join eav_attribute as attr_varchar 
	on attr_varchar.attribute_id = entity_varchar.attribute_id 
	and attr_varchar.attribute_code = '"+ $dbh.escape_string(attr_code) +"'
	where entity_varchar.entity_id = " + $dbh.escape_string(entity_id))
	if row = res.fetch_row then row.shift end
end

def get_decimal_attr(attr_code, entity_id)
	res = $dbh.query("
	select entity_decimal.value 
	from catalog_product_entity_decimal as entity_decimal 
	join eav_attribute as attr_decimal 
	on attr_decimal.attribute_id = entity_decimal.attribute_id 
	and attr_decimal.attribute_code = '"+ $dbh.escape_string(attr_code) +"'
	where entity_decimal.entity_id = " + $dbh.escape_string(entity_id))
	if row = res.fetch_row then row.shift end

end

def get_text_attr(attr_code, entity_id)
	sql = "select entity_text.value 
	from catalog_product_entity_text as entity_text 
	join eav_attribute as attr_text 
	on attr_text.attribute_id = entity_text.attribute_id 
	and attr_text.attribute_code = '"+ $dbh.escape_string(attr_code) +"'
	where entity_text.entity_id = " + $dbh.escape_string(entity_id)
	res = $dbh.query(sql)
	if row = res.fetch_row then row.shift end
end

def get_attr_set(entity_id) 
	res = $dbh.query("
	select attr_set.attribute_set_name
	from catalog_product_entity p
	join eav_attribute_set attr_set 
	on attr_set.entity_type_id = p.entity_type_id 
	and attr_set.attribute_set_id = p.attribute_set_id
	where p.entity_id = " + $dbh.escape_string(entity_id))
	if row = res.fetch_row then row.shift end
end

def get_products(type=nil, entity_ids=nil)
	items = []
	sql = "
	SELECT p.entity_id, p.sku as id, 
	CASE WHEN stock.qty > 0 THEN 'in stock'
	ELSE 'out of stock' END as availability
	FROM catalog_product_entity p
	JOIN cataloginventory_stock_status as stock 
	ON stock.product_id = p.entity_id
	JOIN catalog_product_entity_int as entity_int
	ON entity_int.entity_id = p.entity_id
	JOIN eav_attribute AS attr_int
	ON attr_int.attribute_id = entity_int.attribute_id
	AND attr_int.attribute_code = 'status'
	AND entity_int.value = 1
	WHERE 1"

	if type == 'not-configurable' then
		sql += " AND p.type_id != 'configurable'"
		# exclude simples used in configurables
		sql += " AND p.entity_id NOT IN (
			(SELECT product_id FROM catalog_product_super_link)
		)"
	elsif type then 
		sql += " AND p.type_id = '"+$dbh.escape_string(type)+"'"
	end

	if entity_ids and entity_ids.size > 0 then
		entity_ids.map! do |e| $dbh.quote(e.to_s) end
		sql += " AND p.entity_id in ("+entity_ids.join(',')+")" 
	end

	sql += " ORDER BY 1"

	res = $dbh.query(sql)

	# get their attributes
	res.each do | row |
		item = { 
			:id => row[0], 
			:availability => row[2] 
		}
		%w{ brand gender color }.each do |i|
			item[i.to_sym] = get_int_attr(i, row[0])
		end
		%w{ short_description }.each do |i|
			item[i.to_sym] = get_text_attr(i, row[0]).tr("\n\r", " ")
		end
		%w{ name url_key image condition}.each do |i|
			item[i.to_sym] = get_varchar_attr(i, row[0])
		end
		%w{ price weight }.each do |i|
			item[i.to_sym] = get_decimal_attr(i, row[0])
		end
		item[:product_type] = get_attr_set(row[0])
		items.push(item)
	end
	return items
end

def prepare_item(item)

		# trim name/title
		item[:name] = item[:name].slice(0..69)	

		# prefix URLs with base url
		item[:url_key].insert(0, $base_url).insert(-1, '.html') if(item[:url_key])
		if(item[:image] and item[:image] != 'no_selection') then
			item[:image].insert(0, $base_url+"media/catalog/product")
		else 
			item[:image] = nil
		end

		# change weight to KG
		item[:weight] = "%0.3f" % (item[:weight].to_f / 1000) if(item[:weight])

		# set up price & shipping price for GB
		item[:shipping] = 'GB:::3.00 GBP'
		price = item[:price] 
		item[:price] = 0
		if(price) then
			# truncate price to 2 decimal places
			item[:price] = "%0.2f" % price 

			# set up shipping price for GB
			if item[:price].to_f > 50.00 then
				item[:shipping] = 'GB:::0.00 GBP'
			end
		end

		# make sure condition is set
		unless ['new','used','refurbished'].include?(item[:condition]) then
			item[:condition] = 'new'
		end

		# set up google product category
		if item[:product_type].match(/shoes/i) then
			item[:google_product_category] = 'Apparel & Accessories > Shoes'
		elsif item[:product_type].match(/skateboard/i) then
			item[:google_product_category] = 'Sporting Goods > Outdoor Recreation > Skateboarding'
		elsif $product_type_map.has_key?(item[:product_type]) then
			item[:google_product_category] = $product_type_map.fetch(item[:product_type])
		else 
			item[:google_product_category] = 'Apparel & Accessories > Clothing'
		end

		# preserve column order
		line=[]
		$old_keys.each_index do |i| 
			col = $old_keys[i]
			if col.nil? then col = $keys[i] end
			line.push(item[col]) 
		end
		return line
end

def get_assoc_products(item)
	sql = "
	SELECT entity_id, GROUP_CONCAT(DISTINCT value) as value FROM (
		SELECT i.entity_id, CONCAT(i.attribute_id, '=', i.value) AS value
		FROM catalog_product_super_attribute a  
		JOIN catalog_product_entity_int i ON a.attribute_id = i.attribute_id
		JOIN catalog_product_super_link l ON l.parent_id = a.product_id AND l.product_id = i.entity_id
		WHERE a.product_id = " + $dbh.escape_string(item[:id]) + "
	) a GROUP BY a.entity_id"
	res = $dbh.query(sql)
	associated = []
	if res then
		res.each do |row|
			associated.push(row)
		end
	end
	return associated
end


time = Benchmark.realtime do
	begin
		$dbh = Mysql.real_connect("localhost", user, pass, dbname)
		# get all product entities
		configurable_items = get_products('configurable')
		other_items = get_products('not-configurable')
		puts "Number of configurables returned: #{configurable_items.size}"
		puts "Number of non-configurables returned: #{other_items.size}"

		# gen CSV
		CSV.open(csv_file, 'w', "\t") do |writer|
			writer << $keys
			configurable_items.each do |item|
				
				# write out item
				writer << prepare_item(item)

				# write out associated
				get_assoc_products(item).each do |a|
					if assoc = get_products('simple', [a[0]]).first then
						assoc = prepare_item(assoc)

						# fix URL with product options
						link_index = $old_keys.index(:url_key)
						assoc[link_index] = item[:url_key] + '#' + a[1]

						# set item group id
						#item_group_id_index = $keys.index(:item_group_id)
						#assoc[item_group_id_index] = item[:id]

						writer << assoc
					end
				end
			end

			other_items.each do |item|
				# write out item
				writer << prepare_item(item)
			end

		end
		# send file to GOOGLE
		if send_file then
			puts "sending..."
			ftp = Net::FTP.open(google[:host]) do |ftp|
				ftp.login(google[:user], google[:pass])
				ftp.puttextfile(csv_file)
				ftp.close
			end
		end

	rescue Mysql::Error => e
		puts "Error code: #{e.errno}"
		puts "Error message: #{e.error}"
		puts "Error SQLSTATE: #{e.sqlstate}" if e.respond_to?("sqlstate")
	ensure
		# disconnect from server
		  $dbh.close if $dbh
	end
end
puts "Time elapsed #{time} seconds"
